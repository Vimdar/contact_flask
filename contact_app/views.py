from flask import (
    # render_template,
    request,
    abort,
    jsonify,
)
from flask.views import MethodView
from werkzeug.exceptions import BadRequest
from sqlalchemy.exc import IntegrityError


class ContactView(MethodView):
    def __init__(self, model, related):
        super().__init__()
        self.model = model
        self.related = related

    def get_request_json(self):
        try:
            return request.get_json()
        except BadRequest:
            return None

    def get_append_data(self, data):
        if self.related['model_field'] in data:
            ins = self.related['model'].create(
                **{self.related['rel_model_field']: data.pop(self.related['model_field'])}
            )
            data.update({self.related['model_field']: [ins]})
        return data

    def get(self, username):
        if username is None:
            contacts = self.model.return_all(self.model)
            # return render_template("home.html", contacts=contacts)
            return jsonify({'data': [x.serialize() for x in contacts]})
        con = self.model.get_by_name(self.model, username)
        if con is not None:
            # return {self.model.name: con.first().serialize()}
            return {self.model.name: con.serialize()}
        return abort(404)

    def post(self):
        data = self.get_request_json()
        data = self.get_append_data(data)
        if data is None:
            return {
                'error': f'malformed json contact data'
            }, 400
        try:
            self.model.create(**data)
        except TypeError:
            return {
                'error': f'ivanlid keyword argument for {self.model.name}'
            }, 403  # 422?
        except IntegrityError as e:
            return {
                'error': e.args
            }, 400
        return {'created': True}, 201

    def put(self, username):
        data = self.get_request_json()
        data = self.get_append_data(data)
        if data is None:
            return {
                'error': f'malformed json contact data'
            }, 400
        con = self.model.get_by_name(self.model, username)
        if con is not None:
            if self.related['model_field'] in data:
                con.append(data.pop(self.related['model_field'])[0])
            con.update(**data)
            return {'updated': True}, 204
        return abort(404)

    def delete(self, username):
        con = self.model.get_by_name(self.model, username)
        if con is not None:
            con.delete()
            return {'deleted': True}, 204
        return abort(404)
