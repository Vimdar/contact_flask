from datetime import datetime, timedelta
from flask import (
    make_response,
    jsonify,
)
from .app import create_app
from .tasks import make_celery, id_generator
from .models import (
    Contact,
)


app = create_app()
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379/0',
    CELERY_RESULT_BACKEND='redis://localhost:6379/0',
    # 'CELERY_TASK_SERIALIZER'='json',
)


celery = make_celery(app)
# create flask app; create celery app; populate configs;
# run celery worker and celery beat with option with
# celery -A contact_app.celery worker -B


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(15.0, add_user_task.s(), name='add_random')
    sender.add_periodic_task(60.0, remove_user_task.s(), name='remove_old')


@celery.task(bind=True, name='add_user_task')
def add_user_task(self):
    con = {
        "username": id_generator(),
        "first_name": "first",
        "last_name": "last",
    }
    Contact.create(**con)
    print('Created')
    return {'result': con}


@celery.task(bind=True, name='remove_user_task')
def remove_user_task(self):
    stamp = datetime.now() - timedelta(minutes=1)
    del_query = Contact.get_older(Contact, stamp)
    for row in del_query:
        print(f'deleting {row.username}')
        row.delete()
    print('delete none')
