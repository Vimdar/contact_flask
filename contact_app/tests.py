import logging
import pytest
from flask.testing import FlaskClient

from .app import create_app
from .models import (
    Contact,
    Email
)
from contact_app.extensions import db as _db


@pytest.fixture
def get_app():
    _app = create_app(config_object="contact_app.test_settings")
    _app.logger.setLevel(logging.CRITICAL)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture()
def get_test_client(get_app):
    testing_client = get_app.test_client()
    ctx = get_app.app_context()
    ctx.push()
    yield testing_client
    ctx.pop()


@pytest.fixture
def db(get_app):
    _db.app = get_app
    with get_app.app_context():
        _db.create_all()

    yield _db

    _db.session.close()
    _db.drop_all()


mail = {"email_plain": "foo@bar.com"}
mail2 = {"email_plain": "bar@foo.com"}

contact = {
    "username": "test1",
    "first_name": "first",
    "last_name": "last",
}
contact2 = {
    "username": "test2",
    "first_name": "second",
    "last_name": "last2",
}


@pytest.fixture
def db_users(get_app):
    _db.app = get_app
    with get_app.app_context():
        _db.create_all()
    email = Email(**mail)
    email2 = Email(**mail2)
    con1 = Contact(**contact)
    con1.append(email)
    con2 = Contact(**contact2)
    con2.append(email2)
    _db.session.add(con1)
    _db.session.add(con2)
    _db.session.commit()
    yield _db

    _db.session.close()
    _db.drop_all()


def test_home_page(get_test_client):
    response = get_test_client.get('/contacts')
    assert response.status_code == 200


@pytest.mark.usefixtures('db')
class TestContact:

    def test_get_by_name(self):
        con1 = Contact(**contact)
        email = Email(**mail)
        con1.append(email)
        con1.save()
        retrieved = Contact.get_by_name(Contact, con1.username)
        assert retrieved == con1


@pytest.mark.usefixtures('get_app', 'get_test_client', 'db_users')
class TestEndpoints:
    def test_api(self, get_app, get_test_client):
        assert isinstance(get_test_client, get_app.test_client_class or FlaskClient)

    def test_home_page(self, get_test_client):
        response = get_test_client.get('/contacts')
        assert response.status_code == 200

    def test_get_all(self, db_users, get_test_client):
        response = get_test_client.get('/contacts')
        resp_json = response.json
        data = resp_json['data']
        assert data[0]['emails'][0]['email'] == mail['email_plain']
        del data[0]['emails']
        assert data[0] == contact
        assert data[1]['emails'][0]['email'] == mail2['email_plain']
        del data[1]['emails']
        assert data[1] == contact2

    def test_single(self, db_users, get_test_client):
        response = get_test_client.get('/contacts/test1')
        data = response.json['contact']
        assert data['emails'][0]['email'] == mail['email_plain']
        del data['emails']
        assert data == contact

    def test_post(self, db_users, get_test_client):
        resp = get_test_client.post(
            '/contacts',
            json={
                "username": "test4",
                "first_name": "4",
                "last_name": "4",
                "email": "abv.com"
            }
        )
        assert resp.status_code == 201 and resp.json == {'created': True}
        resp = get_test_client.post(
            '/contacts',
            json={
                "username": "test1",
                "first_name": "4",
                "last_name": "4",
                "email": "abv.com"
            }
        )
        assert resp.status_code == 400
        resp = get_test_client.post(
            '/contacts',
            json={
                "bad_argument": "test1",
            }
        )
        assert resp.status_code == 403 and 'ivanlid keyword argument' in str(resp.data)

    def test_put(self, db_users, get_test_client):
        resp = get_test_client.put(
            '/contacts/test1',
            json={
                "username": "test1",
                "first_name": "4",
                "last_name": "4",
                "email": "abc.com"
            }
        )
        assert resp.status_code == 204
        resp = get_test_client.put(
            '/contacts/test3',
            json={
                "username": "test3",
                "email": "xyz.com"
            }
        )
        assert resp.status_code == 404

    def test_delete(self, db_users, get_test_client):
        resp = get_test_client.delete(
            '/contacts/test1',
            json={
                "username": "test1",
                "first_name": "4",
                "last_name": "4",
                "email": "abc.com"
            }
        )
        assert resp.status_code == 204
        resp = get_test_client.delete(
            '/contacts/test3',
            json={
                "username": "test3",
                "email": "xyz.com"
            }
        )
        assert resp.status_code == 404
