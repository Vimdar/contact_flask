import os


project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = f"sqlite:///{os.path.join(project_dir, 'test_contacts.db')}"

DEBUG = True
SQLALCHEMY_DATABASE_URI = database_file
TESTING = True
