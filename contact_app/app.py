from flask import Flask
from .extensions import db
from .models import (
    Contact,
    Email,
)
from .views import ContactView


def register_extensions(app):
    db.init_app(app)
    with app.app_context():
        db.create_all()


related = {
    'model_field': 'email',
    'model': Email,
    'rel_model_field': 'email_plain',
}
contact_view = ContactView.as_view(
    'contacts_view',
    model=Contact,
    related=related
)


def register_urs_rules(app):
    app.add_url_rule(
        '/contacts',
        view_func=contact_view,
        defaults={'username': None},
        methods=['GET']
    )
    app.add_url_rule('/contacts', view_func=contact_view, methods=['POST'])
    app.add_url_rule(
        '/contacts/<username>',
        view_func=contact_view,
        methods=['GET', 'PUT', 'DELETE']
    )


def create_app(config_object="contact_app.settings"):
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_urs_rules(app)
    return app
