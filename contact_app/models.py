from datetime import datetime, timedelta
from .extensions import db


class CRUDMixin:
    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()


class Model(CRUDMixin, db.Model):
    __abstract__ = True


class Email(Model):
    name = 'email_plain'
    __tablename__ = '_email'
    _email_id = db.Column(db.Integer, primary_key=True)
    email_plain = db.Column(
        db.String(50),
        nullable=False,
    )
    user_id = db.Column(db.Integer, db.ForeignKey('_contacts._contact_id'))

    def serialize(self):
        return {
            "email": self.email_plain,
        }

    def __repr__(self):
        return "<Email: {}>".format(self.email_plain)


class Contact(Model):
    name = 'contact'
    __tablename__ = '_contacts'
    _contact_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(
        db.String(40),
        unique=True,
        nullable=False,
    )
    first_name = db.Column(
        db.String(20),
        nullable=False,
    )
    last_name = db.Column(
        db.String(20),
        nullable=False,
    )
    email = db.relationship(
        "Email",
        backref="contact",
        lazy='dynamic',
    )
    created = db.Column(
        db.DateTime,
        default=datetime.now,
    )

    def return_all(self):
        return self.query.all()

    def get_by_name(self, name):
        con = self.query.filter_by(username=name)
        return con.first() if con.scalar() else None

    def get_older(self, stamp=None):
        if stamp is None:
            stamp = datetime.now() - timedelta(days=1)
        con = self.query.filter(Contact.created < stamp)
        return con

    def append(self, emails):
        self.email.append(emails)

    def serialize(self):
        return {
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "emails": [x.serialize() for x in self.email.all()],
        }

    def __repr__(self):
        return "<Uname: {}>".format(self.username)
